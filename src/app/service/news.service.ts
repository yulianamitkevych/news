import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { News, Article } from "../interface/news";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) {
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<News>('https://newsapi.org/v2/top-headlines?country=ua&apiKey=6f4d44f6dc214b938e81622ff41052dd')
      .pipe(
        map(res => res.articles)
      )
  }
}
