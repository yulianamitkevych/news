import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news',
  template: '<router-outlet></router-outlet>\n',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
