import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  inputs: ['article']
})
export class ArticleComponent implements OnInit {
  article: any;

  constructor(private router: Router) {
    this.article = this.router.getCurrentNavigation()?.extras.state;
  }

  ngOnInit(): void {
  }

  navToNews(): void {
    this.router.navigate(
      [''],
    );
  }

}
