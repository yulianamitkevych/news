import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NewsComponent} from "./news.component";
import {NewsListComponent} from "./news-list/news-list.component";
import {ArticleComponent} from "./article/article.component";

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
    children: [
      {path: '', component: NewsListComponent},
      {path: 'article', component: ArticleComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule {
}
