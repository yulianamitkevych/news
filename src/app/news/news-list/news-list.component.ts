import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";
import { NewsService } from '../../service/news.service';
import { Article } from '../../interface/news';


@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
})
export class NewsListComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  articles: Article[] = [];
  article: Article;

  constructor(
    private newsService: NewsService,
    private route: ActivatedRoute,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.getArticlesList();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  navToArticle(article: Article): void {
    this.router.navigate(
      ['/article'],
      { state: article }
    );
  }

  private getArticlesList(): void {
    this.newsService.getArticles()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe((res) => {
        this.articles = res;
      });
  }
}
